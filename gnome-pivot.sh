#!/bin/bash

# Find user running this
USER=$(whoami)

# Function which reads config file and dumps it to an array
readConfig() {
	local ARRAY="$1"
	local KEY VALUE
	local IFS='='
	declare -g -A "$ARRAY"
	while read; do
		# Filter out comments
		[[ $REPLY == [^#]*[^$IFS]${IFS}[^$IFS]* ]] && {
			read KEY VALUE <<< "$REPLY"
			[[ -n $KEY ]] || continue
			eval "$ARRAY[$KEY]=\"\$VALUE\""
		}
	done
}

# Read config file
readConfig CONFIG < "/home/$USER/.gnome-pivot/gnome-pivot.conf"

# Configuration variables
BG_PATH=${CONFIG[Backgrounds]}	# Background image path
BG_MODE=${CONFIG[Mode]}				# Background mode (none, wallpaper, center, scaled, stretched, zoom, spanned)
INTERVAL=${CONFIG[Interval]}		# Background change interval timer
LOG_PATH=${CONFIG[Log]}				# Log file path
LOG_MAX=${CONFIG[LogMax]}			# Maximum lines for log file
LOG_DATE=$(date)						# Current date for log file

# Set interval variable for timer if run-once paramater (init) is set
if [[ $1 == "init" ]]; then
	echo "$LOG_DATE: gnome-pivot service created for $USER" >> "$LOG_PATH"
	sed -i -e "s/ExecStart=\/bin\/bash/ExecStart=\/bin\/bash\ \/home\/$USER\/.gnome-pivot\/gnome-pivot.sh/g" /home/$USER/.config/systemd/user/gnome-pivot.service

	echo "$LOG_DATE: gnome-pivot timer set to $INTERVAL minutes" >> "$LOG_PATH"
	sed -i -e "s/OnCalendar=.*/OnCalendar=*:0\/$INTERVAL/g" /home/$USER/.config/systemd/user/gnome-pivot.timer

	systemctl --user start gnome-pivot.timer > /dev/null 2>&1
	systemctl --user daemon-reload
fi

# Get our username, then figure out Gnome session ID with it.
# Once that's done we set DBUS_SESSION_BUS_ADDRESS accordingly to be able to target the correct gnome session with gsettings.
USER=$(whoami)
PID=$(pgrep -u $USER gnome-session)
export DBUS_SESSION_BUS_ADDRESS=$(grep -z DBUS_SESSION_BUS_ADDRESS /proc/$PID/environ | cut -d= -f2- | tr '\0' '\n' | sed -e s/DBUS_SESSION_BUS_ADDRESS=//)

# Note: The entire "| tr '\0' '\n' | sed -e s/DBUS_SESSION_BUS_ADDRESS=//" pipe is not required, but was added to prevent "null byte in input" warnings

# Randomly select a JPG or PNG
BG_SEL=$(find "${BG_PATH}" -type f -name "*.jpg" -o -name "*.png" | shuf -n1)

# Set wallpaper mode
gsettings set org.gnome.desktop.background picture-options $BG_MODE

# Rotate wallpaper
gsettings set org.gnome.desktop.background picture-uri "file://$BG_SEL"

# Log what just happened
echo "$LOG_DATE: Wallpaper set to $BG_SEL ($BG_MODE)" >> "$LOG_PATH"

# Trim log to max lines
echo $(tail -n $LOG_MAX $LOG_PATH) > $LOG_PATH
