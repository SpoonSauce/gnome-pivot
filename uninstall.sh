#! /bin/bash

# Don't allow this to be run as root
USER=$(whoami)
if [[ $USER == 'root' ]]; then
	echo "Please run this command as a regular user (i.e., without sudo)!"
	exit 1
fi

# Remove shell script
echo ""
echo "Removing shell script, config and logs..."
rm -r /home/$USER/.gnome-pivot/

# Remove service timer
echo ""
echo "Removing systemd service..."
systemctl --user stop gnome-pivot.timer
systemctl --user disable gnome-pivot.timer

# Remove service and timer
rm /home/$USER/.config/systemd/user/gnome-pivot.service
rm /home/$USER/.config/systemd/user/gnome-pivot.timer
systemctl --user daemon-reload

# Removing autostart script
rm /home/$USER/.config/autostart/gnome-pivot.desktop

# Done
echo ""
echo "Uninstallations complete!"
exit 0
