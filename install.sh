#! /bin/bash

# Don't allow this to be run as root
USER=$(whoami)
if [[ $USER == 'root' ]]; then
	echo "Please run this command as a regular user (i.e., without sudo)!"
	exit 1
fi

# Set paths
CNF_DIR="/home/$USER/.gnome-pivot"
LOG_DIR="/home/$USER/.gnome-pivot/logs"
CNF_PATH="$CNF_DIR/gnome-pivot.conf"
LOG_PATH="$LOG_DIR/gnome-pivot.log"

# Create config directory & file
if [[ ! -d "$CNF_DIR" ]]; then
	mkdir $CNF_DIR
fi
touch $CNF_PATH
chown $USER:$USER $CNF_PATH
chmod 750 $CNF_PATH

# Create log directory & file
if [[ ! -d "$LOG_DIR" ]]; then
	mkdir $LOG_DIR
fi
touch $LOG_PATH
chown $USER:$USER $LOG_PATH
chmod 750 $LOG_PATH

# Install shell script
cp gnome-pivot.sh "$CNF_DIR/"
chmod 755 "$CNF_DIR/gnome-pivot.sh"

# Customize background path
echo ""
read -r -p "Enter background storage path (default: /home/$USER/Pictures): " RESPONSE
if [[ -z $RESPONSE ]]; then
     	RESPONSE="/home/$USER/Pictures"
fi

echo "Background storage path configured to $RESPONSE."

echo "# Path to background image directory" >> $CNF_PATH
echo "Backgrounds=$RESPONSE" >> $CNF_PATH

# Background mode (default: zoom)
echo "" >> $CNF_PATH
echo "# Background mode" >> $CNF_PATH
echo "# Values: none, wallpaper, center, scaled, stretched, zoom, spanned" >> $CNF_PATH
echo "Mode=zoom" >> $CNF_PATH

# Customize timer interval
echo ""
     read -r -p "Enter timer interval in minutes (default: 30): " RESPONSE
     if [[ -z $RESPONSE ]]; then
             RESPONSE=30
     fi

echo "Timer interval set to $RESPONSE minute(s)."

echo "" >> $CNF_PATH
echo "# Timer interval in minutes" >> $CNF_PATH
echo "Interval=$RESPONSE" >> $CNF_PATH

# Log file path
     echo "" >> $CNF_PATH
     echo "# Log file path (change at your own risk!)" >> $CNF_PATH
     echo "Log=$LOG_PATH" >> $CNF_PATH

# Log file max lines (default: 100)
echo "" >> $CNF_PATH
echo "# Log file max lines" >> $CNF_PATH
echo "LogMax=100" >> $CNF_PATH

# Configuration file's done!
echo ""
echo "This can be changed at any time by modifying $CNF_PATH"

# Install service and timer
echo ""
echo "Installing systemd service..."
if [[ ! -d "/home/$USER/.config/systemd/" ]]; then
	mkdir /home/$USER/.config/systemd/
fi
if [[ ! -d "/home/$USER/.config/systemd/user/" ]]; then
	mkdir /home/$USER/.config/systemd/user/
fi
cp gnome-pivot.service /home/$USER/.config/systemd/user
cp gnome-pivot.timer /home/$USER/.config/systemd/user

# Install autostart script
echo ""
echo "Installing autostart script and initializing gnome-pivot..."
cp gnome-pivot.desktop /home/$USER/.config/autostart
/home/$USER/.gnome-pivot/gnome-pivot.sh init

# Enable service timer
systemctl --user start gnome-pivot.timer
systemctl --user enable gnome-pivot.timer

# Done
echo ""
echo "Installation complete!"
exit 0
