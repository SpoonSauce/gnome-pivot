# gnome-pivot
Simple, lightweight background image rotation service for GNOME desktop.

## Installation
Run `./install.sh` as the user you wish to install the service for.

You will be prompted to enter an absolute path to your backgrounds folder and an interval (in minutes) at which the service timer should run.

## Configuration
The installation script will configure the service.  Should you wish to modify these values the config file will be located at `/home/username/.gnome-pivot/gnome-pivot.conf`.

```
# Path to background image directory
Backgrounds=/home/username/Pictures

# Background mode
# Values: none, wallpaper, center, scaled, stretched, zoom, spanned
Mode=zoom

# Timer interval in minutes
Interval=30

# Log file path (change at your own risk!)
Log=/home/username/.gnome-pivot/logs/gnome-pivot.log

# Log file max lines
LogMax=100
```

## Removal
Run `./uninstall.sh` as the user you wish to uninstall the service for.

To disable the service temporarily as opposed to completely uninstalling the script, simply disable the systemd timer:

```
systemctl --user stop gnome-pivot.timer
systemctl --user disable gnome-pivot.timer
```

To re-enable the service:

```
systemctl --user enable gnome-pivot.timer
systemctl --user start gnome-pivot.timer
```

## Known Issues

* Logs appear in a single line rather than one entry per line
